# Apache Airflow example

#### Implemented operators (Plugins)
  - Download operator (plugins/download.py) - Download files 
  - Upload AWS S3 operator (plugins/s3Upload.py) - Upload files on S3 drive
  - Google sheet read operator (plugins/readGoogleSheet.py) - Read and convert google sheet to json file

#### Implemented DAG's
  - Downloading parking details (dags/downloadParkings.py)
  - Trigger EMR with Spark to processes parking details from aws s3 drive (dags/emr.py)

#### PreRequirements
  - Installed PostgreSQL
  - Installed Apache Airflow
  - Created and configurated AWS service account (key, secret) for AWS S3
  - Created and configurated AWS service account (key, secret) for AWS EMR
  - Create google service account
  - Enabled Google sheet API and download google.json configuration file in project/dags folder
  - Share google sheets with google service account

#### Airflow -> Connection -> Connections
  - aws_s3
  - aws_default
  - emr_default

#### Apache airflow configuration

  - airflow_home - project_path/apache.airflow.example
  - dags_folder - project_path/apache.airflow.example/dags
  - base_log_folder - project_path/apache.airflow.example/logs
  - dag_processor_manager_log_location - project_path/apache.airflow.example/logs/dag_processor_manager
  - plugins_folder=project_path/apache.airflow.example/plugins
  - executor=LocalExecutor - support parallel processing
  - sql_alchemy_conn=postgresql+psycopg2://airflow:airflow@localhost:5432/airflow
  - parallelism=8 - max active tasks in same time 
  - dag_concurrency=4 - max active dags in same time
  
## DAG's
#### Download parking
![picture](img/parking.png)

#### Trigger AWS EMR
![picture](img/emr.png)
