# airflow related
from airflow import DAG

from airflow.contrib.operators.emr_create_job_flow_operator \
    import EmrCreateJobFlowOperator
from airflow.contrib.operators.emr_add_steps_operator \
    import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_terminate_job_flow_operator \
    import EmrTerminateJobFlowOperator

# other packages
from datetime import datetime
from datetime import timedelta

default_args = {
    'owner': "airflow",
    'depends_on_past': False,
    'start_date': datetime(2019, 2, 14),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=5),
}

dag = DAG(
  dag_id='emr_parking_processor',
  description='EMR parking processor',
  schedule_interval=None,
  max_active_runs=1,
  catchup=False,
  default_args=default_args)

emr_defaults = {
    "Name": "EMR parking processor",
    "Configurations": [
        {
          "Classification": "spark",
          "Properties": {
            "maximizeResourceAllocation": "true"
          }
        },
        {
          "Classification": "yarn-site",
          "Properties": {
            "yarn.nodemanager.vmem-check-enabled": "false"
          }
        },
        {
          "Classification": "spark-env",
          "Configurations": [
            {
              "Classification": "export",
              "Properties": {
                "PYSPARK_PYTHON": "/usr/bin/python3",
                "SPARK_ENV": "development"
              }
            }
          ]
        },
        {
          "Classification": "yarn-env",
          "Configurations": [
            {
              "Classification": "export",
              "Properties": {
                "PYSPARK_PYTHON": "/usr/bin/python3",
                "SPARK_ENV": "development"
              }
            }
          ]
        }
      ],
      "BootstrapActions": [
        {
          "Name": "install_python_modules",
          "ScriptBootstrapAction": {
            "Path": "s3://spark-deploys/python-modules.sh"
          }
        }
      ]
}
with dag:
    creator = EmrCreateJobFlowOperator(
        task_id='create',
        job_flow_overrides=emr_defaults,  # added local configuration
        aws_conn_id='aws_default',
        emr_conn_id='emr_default',
        dag=dag
    )

    add = EmrAddStepsOperator(
                task_id='add_step_parking_processor',
                execution_timeout=timedelta(hours=3),
                job_flow_id="{{ task_instance.xcom_pull('create', key='return_value') }}",
                aws_conn_id='aws_default',
                steps=[{
                    "Name": "operators",
                    "ActionOnFailure": "CONTINUE",
                    "HadoopJarStep": {
                        "Jar": "command-runner.jar",
                        "Args": [
                                    "spark-submit",
                                    "--master",
                                    "yarn",
                                    "--deploy-mode",
                                    "cluster",
                                    "--packages",
                                    "com.amazonaws:aws-java-sdk:1.11.572,org.apache.spark:spark-streaming-kinesis-asl_2.11:2.4.3,com.amazonaws:dynamodb-streams-kinesis-adapter:1.4.0,org.mongodb.spark:mongo-spark-connector_2.11:2.4.0,org.apache.hadoop:hadoop-aws:3.2.0,com.fasterxml.jackson.core:jackson-core:2.9.9,com.fasterxml.jackson.core:jackson-databind:2.9.9",
                                    "--py-files",
                                    "s3://spark-deploys/data.zip",
                                    "s3://spark-deploys/transform.py",
                                    "all"
                                ]
                    }
                }],
                dag=dag
            )

    check = EmrStepSensor(
            task_id='check_parking_processor',
            job_flow_id="{{ task_instance.xcom_pull('create', key='return_value') }}",
            step_id="{{ task_instance.xcom_pull('add_step_parking_processor', key='return_value')[0] }}",
            aws_conn_id='aws_default',
            dag=dag
        )

    remover = EmrTerminateJobFlowOperator(
        task_id='remove',
        trigger_rule="all_done",
        job_flow_id="{{ task_instance.xcom_pull('create', key='return_value') }}",
        aws_conn_id='aws_default',
        dag=dag
    )

creator >> add >> check >> remover