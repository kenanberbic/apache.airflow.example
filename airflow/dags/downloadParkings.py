import os
from airflow import DAG
from airflow.models import Variable
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators import \
    DownloadDataOperator, \
    S3UploadOperator,\
    ReadGoogleSheetOperator

# other packages
from datetime import datetime
from datetime import timedelta

PATH = os.path.dirname(os.path.abspath(__file__))

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 2, 14),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 3,
    'retry_delay': timedelta(seconds=5),
}

dag = DAG(
  dag_id='parking_places',
  description='Download parking places',
  schedule_interval='@daily',
  catchup=False,
  default_args=default_args)

def conditional_rule(context, dag_run_obj):
    return dag_run_obj

with dag:
    download_bruxelles_parkings = DownloadDataOperator(url="https://data.opendatasoft.com/explore/dataset/bruxelles_parkings_publics@bruxellesdata/download/?format=json&timezone=Europe/Berlin",
                                                       path="bruxelles_parkings.json",
                                                       python_callable=lambda: None,
                                                       task_id='download_bruxelles_parkings')

    s3_upload_bruxelles_parkings = S3UploadOperator(task_id='s3_upload_bruxelles_parkings',
                                                    aws_conn_id="aws_s3",
                                                    files=[{"file": "bruxelles_parkings.json", "virtual": "bruxelles_parkings"}],
                                                    bucket_name="p.static")

    download_saemes_parkings = DownloadDataOperator(url="https://data.opendatasoft.com/explore/dataset/places-disponibles-parkings-saemes@saemes/download/?format=json&timezone=Europe/Berlin",
                                                    path="saemes_parkings.json",
                                                    python_callable=lambda: None,
                                                    task_id='download_saemes_parkings')

    s3_upload_saemes_parkings = S3UploadOperator(task_id='s3_upload_saemes_parkings',
                                                 aws_conn_id="aws_s3",
                                                 files=[{"file": "saemes_parkings.json", "virtual": "saemes_parkings"}],
                                                 bucket_name="p.static")

    download_france_parkings = DownloadDataOperator(url="https://data.opendatasoft.com/explore/dataset/20170419_res_fichesequipementsactivites@datailedefrance/download/?format=json&timezone=Europe/Berlin",
                                                    path="france_parkings.json",
                                                    python_callable=lambda: None,
                                                    task_id='download_france_parkings')

    s3_upload_france_parkings = S3UploadOperator(task_id='s3_upload_france_parkings',
                                                 aws_conn_id="aws_s3",
                                                 files=[{"file": "france_parkings.json", "virtual": "france_parkings"}],
                                                 bucket_name="p.static")

    read_parking_google_sheet = ReadGoogleSheetOperator(secretFile="{}/google.json".format(PATH),
                                                        sheetId="15g19qJ7p0kfJU8MMX_Ff5Ds4pnKNuTeX2IWK6UJTBnw",
                                                        sheetName="parking",
                                                        python_callable=lambda: None,
                                                        path="user_defined.json",
                                                        task_id="read_user_defined_parking")

    s3_upload_sheet_parking = S3UploadOperator(task_id='s3_upload_user_defined_parking',
                                               aws_conn_id="aws_s3",
                                               files=[{"file": "user_defined.json", "virtual": "user_defined"}],
                                               bucket_name="p.static")

    trigger = TriggerDagRunOperator(
        task_id='trigger_emr_fl_and_faa',
        trigger_dag_id="emr_parking_processor",
        trigger_rule="all_done",
        python_callable=conditional_rule,
        dag=dag,
    )

download_bruxelles_parkings >> s3_upload_bruxelles_parkings >> trigger
download_saemes_parkings >> s3_upload_saemes_parkings >> trigger
download_france_parkings >> s3_upload_france_parkings >> trigger
read_parking_google_sheet >> s3_upload_sheet_parking >> trigger