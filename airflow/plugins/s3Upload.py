import logging
import os
import hashlib
import pathlib
from airflow.hooks.S3_hook import S3Hook
from datetime import datetime
from airflow.operators.python_operator import PythonOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

log = logging.getLogger(__name__)
tmpPath = "/var/tmp/parking"

class S3UploadOperator(PythonOperator, S3Hook):
    @apply_defaults
    def __init__(self, aws_conn_id, bucket_name, python_callable=lambda: None, files=[], folder=None, latest=True, virtual=None, *args, **kwargs):
        super(S3UploadOperator, self).__init__(python_callable=python_callable, *args, **kwargs)
        S3Hook.__init__(self, aws_conn_id=aws_conn_id)

        self.bucket_name = bucket_name
        self.paths = []
        if folder:
            for r, d, f in os.walk(os.path.join(tmpPath, folder)):
                for file in f:
                    self.paths.append({"source": os.path.join(r, file),
                                       "destination": virtual + "/" + file.split(".")[0] + "/" + datetime.utcnow().strftime('%Y%m%d'),
                                       "latest": latest})
        else:
            for file in files:
                self.paths.append({"source": os.path.join(tmpPath, file["file"]),
                                   "destination": file["virtual"],
                                   "latest": latest})

    def execute(self, context):
        log.info("STARTING SAVING ON S3")

        for path in self.paths:
            extItems = path["source"].split(".")
            ext = extItems[len(extItems) - 1].split("-")

            key = ext[1] if len(ext) == 2 else \
                hashlib.md5(pathlib.Path(path["source"]).read_bytes()).hexdigest()

            self.load_file(bucket_name=self.bucket_name,
                           filename=path["source"],
                           key=path["destination"] + "/" + key + "." + ext[0], replace=True)

            if(path["latest"]):
                self.load_file(bucket_name=self.bucket_name,
                               filename=path["source"],
                               key=path["destination"] + "/" + "latest." + ext[0], replace=True)

        context["paths"] = self.paths
        super(S3UploadOperator, self).execute(context)

        log.info("FINISHED SAVING ON S3")


class S3UploadPlugin(AirflowPlugin):
    name = "file_to_s3"
    operators = [S3UploadOperator]