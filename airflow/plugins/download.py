import logging
import os
import pathlib

from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

import requests

log = logging.getLogger(__name__)
tmpPath = "/var/tmp/parking"

header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
  "X-Requested-With": "XMLHttpRequest"
}

class DownloadDataOperator(BaseOperator):
    @apply_defaults
    def __init__(self, url, path, parse_callable=None, python_callable=None, **kwargs):
        self.url = url
        pathlib.Path(tmpPath).mkdir(parents=True, exist_ok=True)
        self.path = os.path.join(tmpPath, path)
        self.parse_callable = parse_callable
        self.python_callable = python_callable
        super(DownloadDataOperator, self).__init__(**kwargs)

    def execute(self, context):
        log.info("START WITH DOWNLOADING %s", self.url)
        download = requests.get(self.url, headers=header, allow_redirects=True)
        content = self.parse_callable(download.content) if self.parse_callable else download.content
        open(self.path, 'wb').write(content)
        if self.python_callable:
            self.python_callable(self.path)
        log.info("WRITTING FILE TO %s", self.path)


class DownloadDataPlugin(AirflowPlugin):
    name = "download_data"
    operators = [DownloadDataOperator]