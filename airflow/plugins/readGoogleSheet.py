import logging
import os
import pathlib
import json

from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.operators.python_operator import PythonOperator

import gspread
from oauth2client.service_account import ServiceAccountCredentials

log = logging.getLogger(__name__)
tmpPath = "/var/tmp/parking"

SCOPE = ['https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive.file",
         "https://www.googleapis.com/auth/drive"]

class ReadGoogleSheetOperator(PythonOperator):
    @apply_defaults
    def __init__(self, secretFile, sheetId, sheetName, path=None, provide_context=False, **kwargs):
        creds = ServiceAccountCredentials.from_json_keyfile_name(secretFile, SCOPE)
        self.client = gspread.authorize(creds)
        self.sheetId = sheetId
        self.sheetName = sheetName
        self.provide_context = provide_context

        if path:
            pathlib.Path(tmpPath).mkdir(parents=True, exist_ok=True)
            self.path = os.path.join(tmpPath, path)

        super(ReadGoogleSheetOperator, self).__init__(**kwargs)

    def execute(self, context):
        log.info("START WITH DOWNLOADING %s %s", self.sheetId, self.sheetName)
        sheet = self.client.open_by_key(self.sheetId).sheet1

        items = sheet.get_all_records()
        if self.path:
            open(self.path, 'wb').write(json.dumps(items).encode())
        elif self.provide_context:
            context["sheet_records"] = sheet.get_all_records()

        super(ReadGoogleSheetOperator, self).execute(context)
        log.info("FINISHED FOR SHEET %s", self.sheetId)


class ReadGoogleSheetPlugin(AirflowPlugin):
    name = "read_google_sheet"
    operators = [ReadGoogleSheetOperator]